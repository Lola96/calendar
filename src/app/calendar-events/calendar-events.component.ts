import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Moment } from 'moment';
import { MatDatepicker, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { UserEvent } from '../models/userEvent';
import { UserService } from '../services/user.service';


export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-calendar-events',
  templateUrl: './calendar-events.component.html',
  styleUrls: ['./calendar-events.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class CalendarEventsComponent implements OnInit {

  public eventForm: FormGroup;
  date = new FormControl();
  isCheckedSecond: boolean;
  isCheckedFirst: boolean;
  userId: number;
  
  
  constructor(private dialog: MatDialog,
              private userService: UserService) {}

  ngOnInit() {
    this.eventForm = new FormGroup({
      eventName: new FormControl('', [Validators.required, Validators.maxLength(30), Validators.minLength(2)]),
      members: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      date: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    });
    this.isCheckedFirst = false;
    this.isCheckedSecond = false;
    this.userId =+ sessionStorage.getItem('userId');
  }

  cancel() {
    this.dialog.closeAll();
  }

  add(){
    const userEvent: UserEvent = new UserEvent();
    
    userEvent.date = this.eventForm.value.date;
    userEvent.description = this.eventForm.value.eventName;

    this.userService.addEventForStudent(this.userId,userEvent)
    .toPromise()
    .then(()=> console.log('udało się dodać wydarzenie')
    )
    .catch(()=> console.log('nie udało się dodać wydarzenia'))
    .finally(()=> {
      window.location.reload(); 
      this.dialog.closeAll();
    });
    
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }
  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.eventForm.value.date;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.eventForm.value.date;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  onChangeFirst(){
    if(this.isCheckedSecond){
      this.isCheckedSecond = false;
    } else {
      this.isCheckedSecond = true;
    }
  }
  onChangeSecond(){

    if(this.isCheckedFirst === true){
      this.isCheckedFirst = false;
    } else {

      this.isCheckedFirst = true;
    }
  }
}
