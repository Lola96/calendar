import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { getDefaultService } from 'selenium-webdriver/opera';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userId: number;
  user: User;

  @Output() public sidenavToggle = new EventEmitter();
  constructor(   private authenticationService: AuthenticationService,
    private userService: UserService) { }

  ngOnInit() {
    this.userId =+ sessionStorage.getItem('userId');
    this.getUser();
  }

  getUser(){
    this.userService.getUser(this.userId)
    .toPromise()
    .then(data => {
       this.user = data;   
    })
    .catch(()=> console.log('nie udało się pobrać studenta')
    )
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  logout() {
    sessionStorage.clear();
    location.reload();
  }

}
