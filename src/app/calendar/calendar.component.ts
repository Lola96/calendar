import { Component, OnInit } from '@angular/core';
import { CalendarEventsComponent } from '../calendar-events/calendar-events.component';
import {MatDialog, MatTableDataSource} from '@angular/material';
import { UserService } from '../services/user.service';
import { UserEvent } from '../models/userEvent';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  userId: number;
  events: UserEvent[];
  dataSource:any;
  displayedColumns: string[] = ['description', 'date'];
  constructor(private dialog: MatDialog,
    private userService: UserService) {}

  ngOnInit() {
    this.userId =+ sessionStorage.getItem('userId');
    this.getEvents();
  }

  getEvents(){
    this.userService.getStudentsEvent(this.userId)
    .toPromise()
    .then(data => {
        this.events = data;
        this.dataSource = new MatTableDataSource(data);
    })
    .catch(()=> console.log('nie udało sie pobrać eventów')
    )
  }

  selectedDate: any;

  onSelect(){
    // console.log(event);
    // this.selectedDate= event;
    this.openDialog();
  }

  openDialog(): void {
    this.dialog.open(CalendarEventsComponent,{
      panelClass: 'dialog-container',
      disableClose: true,
      position: { top: "10px" }});         
  }
  
}
