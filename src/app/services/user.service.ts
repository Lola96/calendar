import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { UserEvent } from 'src/app/models/userEvent';
import { UserGroup } from '../models/userGroup';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
  getUser(id: number) : Observable<any>{
    return this.http.get<User>('http://localhost:8080/api/auth/' + id);
  }

  addEventForStudent(id: number, event: UserEvent){
    return this.http.post('http://localhost:8080/api/auth/userEvent/' + id, event);
  }

  getStudentsEvent(id: number): Observable<any[]>{
    return this.http.get<Event[]>('http://localhost:8080/api/auth/studentsEvents/' + id);
  }
  getUserGroups(id: number): Observable<any> {
    return this.http.get<UserGroup>('http://localhost:8080/api/auth/getGroups/' + id);
  }
  addGroup(id: number, userGroup: UserGroup){
    return this.http.post('http://localhost:8080/api/auth/userGroup/' + id, userGroup);
  }
}
