import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { TokenStorage } from '../helpers/token.storage';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private token: TokenStorage) {}

  public isAuthenticated(): boolean {
    
    const token = this.token.getToken();
    return token != null;
  }

  loginClient(email: string, password: string) {
    return this.http.post<any>('http://localhost:8080/api/auth/signin', { email, password });
  }

  register(user: User) {
    return this.http.post(`http://localhost:8080/api/auth/signup`, user);
  }

  logout() {
    this.token.signOut();
  }
}
