import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserGroup } from '../models/userGroup';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

 public myForm: FormGroup;
 userId: number;

  constructor(private dialog: MatDialog,
              private userService: UserService) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      members: new FormControl('', [Validators.required, Validators.maxLength(60)]),
    });
    this.userId = +sessionStorage.getItem('userId');
  }

  add(){
    const userGroup: UserGroup = new UserGroup();
    userGroup.groupName = this.myForm.value.name;

    this.userService.addGroup(this.userId, userGroup)
    .toPromise()
    .then(()=> console.log('udało się dodać grupe')
    )
    .catch(()=> console.log('nie udało się dodać grupy'))
    .finally(()=> {
      window.location.reload();
      this.dialog.closeAll();
    });
  }
  cancel() {
    this.dialog.closeAll();
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.myForm.controls[controlName].hasError(errorName);
  }
}
