export class User {
    email: string;
    password: string;
    displayName: string;
    department: string;
    collage: string;
    course: string;
    semester: string;
    photo:string;
    constructor() {}
}
