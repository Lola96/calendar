import { Component, OnInit , Inject} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import { GroupComponent } from '../group/group.component';
import { UserService } from 'src/app/services/user.service';
import { User } from '../models/user';
import { UserGroup } from '../models/userGroup';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

   userId: number;
   user: User;
   groups: UserGroup[];
   dataSource: any;
   displayedColumns: string[] = ['groupName'];

  constructor(private dialog: MatDialog,
              private userService: UserService) {}

  ngOnInit(){
    this.userId =+ sessionStorage.getItem('userId');
    this.getUser();
    this.getGroups();
  }


  getUser(){
    this.userService.getUser(this.userId)
    .toPromise()
    .then(data => {
       this.user = data;
    })
    .catch(()=> console.log('nie udało się pobrać studenta')
    )
  }

  openDialog(): void {
    this.dialog.open(GroupComponent,{
      panelClass: 'dialog-container',
      disableClose: true,
      position: { top: "10px" }});
  }

  getGroups(){
    this.userService.getUserGroups(this.userId)
    .toPromise()
    .then(data => {
        this.groups = data;
        this.dataSource = new MatTableDataSource(data);
    })
    .catch(()=> console.log('nie udało sie pobrać eventów')
    )
  }
}
